# ansible_grafana
This ansible playbook config sets up a grafana instance on a Debian 10 (Buster) server.

### 0. Get Debian 10 server and register dns records
### 1. Fill variables in "group_vars/all"

### 2. Add server ip addresses + domain name to "./hosts"

### 3. Connect via vpn to the trusted_proxy_ip

If you skip this step, ufw might block your ssh session during execution

### 4. Run ansible playbook
```bash
ansible-playbook -i hosts site.yml
```
